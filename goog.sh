#!/bin/bash

#This program assumes users will actually enter a website for the second arguement

checkwebsite(){
    wget -q -O website.html $2
    occurences=$(sed 's/$1/$1\n/g' website.html | grep -c $1)
    echo "$1: $occurences"
}

if [[(-n "$1") && (-n "$2") && (-z "$3")]]; then
    (checkwebsite $1 $2)
else
    echo "Usage goog.sh <WORD> <WEBSITE>"
fi

