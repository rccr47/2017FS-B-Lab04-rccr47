#!/bin/bash

printmenu(){
  echo "v) View $1"
  echo "e) Edit $1"
  echo "c) Compile $1"
  echo "q) Quit"
}

for entry in *; do
  (printmenu $entry)
  read input

  case $input in
       v)
          less $entry
          ;;
       e)
          jpico $entry
          ;;
       c)
          g++ $entry
          ;;
       q)
          break
          ;;
       *)
          echo "Invalid Option. Skipping file: $entry"
          ;;
  esac
done
